"=============================================================
" Command Line Argument:
"=============================================================
let args=""

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Default plugin config
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

function! s:spacevim_preinstall()
  " nerdcommenter {{{
  let g:NERDCreateDefaultMappings = get(g:, 'NERDCreateDefaultMappings', 0)
  " }}}

  " vim-gitgutter {{{
  let g:gitgutter_map_keys = get(g:, 'gitgutter_map_keys', 0)
  " }}}

  " vim-startify {{{
  let g:startify_custom_header = get(g:, 'startify_custom_header', [
  \'',
  \'',
  \' /$$      /$$      /$$$$$$      /$$   /$$       /$$    /$$ /$$                ',
  \' | $$$    /$$$     /$$__  $$    | $$  /$$/      | $$   | $$|__/               ',
  \' | $$$$  /$$$$    | $$  \ $$    | $$ /$$/       | $$   | $$ /$$ /$$$$$$/$$$$  ',
  \' | $$ $$/$$ $$    | $$$$$$$$    | $$$$$/        |  $$ / $$/| $$| $$_  $$_  $$ ',
  \' | $$  $$$| $$    | $$__  $$    | $$  $$         \  $$ $$/ | $$| $$ \ $$ \ $$ ',
  \' | $$\  $ | $$    | $$  | $$    | $$\  $$         \  $$$/  | $$| $$ | $$ | $$ ', 
  \' | $$ \/  | $$ /$$| $$  | $$ /$$| $$ \  $$         \  $/   | $$| $$ | $$ | $$ ',
  \' |__/     |__/|__/|__/  |__/|__/|__/  \__/          \_/    |__/|__/ |__/ |__/ ',
  \'',
  \'',
  \ ])
  " }}}

  " vim-swoop {{{
  let g:swoopUseDefaultKeyMap = get(g:, 'swoopUseDefaultKeyMap', 0)
  " }}}
endfunction

function! s:spacevim_postinstall()
  " vim-arpeggio {{{
  if exists('g:loaded_arpeggio')
    if exists('g:dotspacevim_escape_key_sequence')
      call arpeggio#map('i', '', 0, g:dotspacevim_escape_key_sequence, '<Esc>')
    endif
    let g:arpeggio_timeoutlen = get(g:, 'arpeggio_timeoutlen', 100)
  endif
  " }}}

  " vim-leader-guide {{{
  if exists('g:loaded_leaderGuide_vim')
    function! s:spacevim_displayfunc()
      let g:leaderGuide#displayname = substitute(g:leaderGuide#displayname, '\c<cr>$', '', '')
      let g:leaderGuide#displayname = substitute(g:leaderGuide#displayname, '^<SID>', '', '')
      let g:leaderGuide#displayname = substitute(g:leaderGuide#displayname, '#', '', '')
    endfunction
    if exists('g:leaderGuide_displayfunc')
      call add(g:leaderGuide_displayfunc, function('s:spacevim_displayfunc'))
    else
      let g:leaderGuide_displayfunc = [function('s:spacevim_displayfunc')]
    endif

    call leaderGuide#register_prefix_descriptions('<Space>', 'g:lmap')
    nnoremap <silent> <leader> :<c-u>LeaderGuide '<Space>'<CR>
    vnoremap <silent> <leader> :<c-u>LeaderGuideVisual '<Space>'<CR>
  endif
  " }}}
endfunction

call s:spacevim_preinstall()
augroup spacevim_postinstall
  autocmd!
  autocmd VimEnter * call s:spacevim_postinstall()
augroup END

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Helpers
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! s:spacevim_bind(map, binding, name, value, isCmd)
  if a:isCmd
    let l:value = ':' . a:value . '<cr>'
  else
    let l:value = a:value
  endif
  if a:map ==# 'map' && maparg('<Leader>' . a:binding, '') ==# ''
    let l:noremap = 'noremap'
  elseif a:map ==# 'nmap' && maparg('<Leader>' . a:binding, 'n') ==# ''
    let l:noremap = 'nnoremap'
  elseif a:map ==# 'vmap' && maparg('<Leader>' . a:binding, 'v') ==# ''
    let l:noremap = 'vnoremap'
  else
    let l:noremap = ''
  endif

  if l:noremap !=# ''
    execute l:noremap . ' <silent> <SID>' . a:name . '# ' . l:value
    execute a:map . ' <Leader>' . a:binding . ' <SID>' . a:name . '#'
  endif
endfunction

function! s:spacevim_bind_plug(map, binding, name, value)
  if a:map ==# 'map' && maparg('<Leader>' . a:binding, '') ==# ''
    let l:map = 'map'
  elseif a:map ==# 'nmap' && maparg('<Leader>' . a:binding, 'n') ==# ''
    let l:map = 'nmap'
  elseif a:map ==# 'vmap' && maparg('<Leader>' . a:binding, 'v') ==# ''
    let l:map = 'vmap'
  else
    let l:map = ''
  endif

  if l:map !=# ''
    execute l:map . ' <silent> <SID>' . a:name . '# <Plug>' . a:value
    execute a:map . ' <Leader>' . a:binding . ' <SID>' . a:name . '#'
  endif
endfunction

"""""""""""""""""""""""
" Bindings
"""""""""""""""""""""""
  let g:lmap.w = { 'name': '+windows' }
  call s:spacevim_bind('map', 'w-', 'split-window-below', 'split', 1)
  call s:spacevim_bind('map', 'w/', 'split-window-right', 'vsplit', 1)
  call s:spacevim_bind('map', 'w=', 'balance-windows', 'wincmd =', 1)
  call s:spacevim_bind('map', 'wc', 'delete-window', 'q', 1)
  call s:spacevim_bind('map', 'wh', 'window-left', 'wincmd h', 1)
  call s:spacevim_bind('map', 'wH', 'window-move-far-left', 'wincmd H', 1)
  call s:spacevim_bind('map', 'wj', 'window-down', 'wincmd j', 1)
  call s:spacevim_bind('map', 'wJ', 'window-move-far-down', 'wincmd J', 1)
  call s:spacevim_bind('map', 'wk', 'window-up', 'wincmd k', 1)
  call s:spacevim_bind('map', 'wK', 'window-move-far-up', 'wincmd K', 1)
  call s:spacevim_bind('map', 'wl', 'window-right', 'wincmd l', 1)
  call s:spacevim_bind('map', 'wL', 'window-move-far-right', 'wincmd L', 1)
  call s:spacevim_bind('map', 'wm', 'maximize-buffer', 'call SpacevimMaximizeBuffer()', 1)
  call s:spacevim_bind('map', 'ws', 'split-window-below', 'split', 1)
  call s:spacevim_bind('map', 'wS', 'split-window-below-and-focus', 'split\|wincmd w', 1)
  call s:spacevim_bind('map', 'wv', 'split-window-right', 'vsplit', 1)
  call s:spacevim_bind('map', 'wV', 'split-window-right-and-focus', 'vsplit\|wincmd w', 1)
  call s:spacevim_bind('map', 'ww', 'other-window', 'wincmd w', 1)

  let g:lmap.l = { 'name': '+lua' }
  call s:spacevim_bind('map', 'le', 'run-lua-file', 'call SpacevimRunLuaFile()', 1)
  call s:spacevim_bind('map', 'lc', 'compile-lua-file', 'call SpacevimCompileLuaFile()', 1)
  call s:spacevim_bind('map', 'lC', 'compile-lua-project', 'call SpacevimCompileLuaProject()', 1)

  let g:lmap.b = { 'name': '+buffers' }
  call s:spacevim_bind('map', 'be', 'fullscreen-buffers', 'BufExplorer', 1)
  call s:spacevim_bind('map', 'bt', 'toggle-buffers', 'ToggleBufExplorer', 1)
  call s:spacevim_bind('map', 'bs', 'horizontal-buffers', 'BufExplorerHorizontalSplit', 1)
  call s:spacevim_bind('map', 'bv', 'vertical-buffers', 'BufExplorerVerticalSplit', 1)
  
function! s:SendToShell(command)
  :exec("!" . "printf \"\033c\"; " . a:command)
endfunction

function! s:RunCmd(command)
  return 'echo "$ ' . a:command . '" && ' . a:command
endfunction

function! s:RunChainCmd(command)
  return 'echo "$ ' . a:command . '" && ' . a:command . " && "
endfunction

function! SpacevimRunLuaFile()
  call s:SendToShell(s:RunCmd ("lua " . g:thisFileName))
endfunction

function! SpacevimCompileLuaFile()
  call s:SendToShell(
    \ s:RunChainCmd ('luac -o ' . g:thisFileNameNoExt. '.bin %') 
    \ . s:RunCmd ("lua " . g:thisFileNameNoExt . ".bin")
  \)
endfunction

function! SpacevimCompileLuaProject()
  if exists(finddir('lib'))
    call s:SendToShell(
      \ s:RunChainCmd ('luastatic ' . g:thisFileName . ' lib/*.lua') 
      \ . s:RunCmd ("./" . g:thisFileNameNoExt)
    \)
  elseif exists(finddir('../lib'))
    call s:SendToShell(
      \ s:RunChainCmd ('luastatic ' . g:thisFileName . ' ../lib/*.lua') 
      \ . s:RunCmd ("./" . g:thisFileNameNoExt)
    \)
  else
    call s:SendToShell(
      \ s:RunChainCmd ('luastatic ' . g:thisFileName) 
      \ . s:RunCmd ("./" . g:thisFileNameNoExt)
    \)
  endif
endfunction
